# 导包
import requests

import app
from api.ihrm_login import LoginApi


class Department(object):
    # 创建一个部门类
    def __init__(self):
        self.url = app.BASE_URL + app.DEPARTMENT_PATH
        login = LoginApi()
        json = {"mobile": "13800000002",
                "password": "123456"}
        response = login.login(json)
        # print(response.json())
        self.Authorization = response.json().get('data')
        self.headers = {'Authorization': self.Authorization}

    # 部门列表查询
    def department_list_query(self):
        return requests.get(url=self.url,
                            headers=self.headers)
        # print(response.json())

    # 部门查询
    # def department_
    def department_query(self, id):
        self.url = self.url + "/" + id
        return requests.get(url=self.url,
                            headers=self.headers)
        # print(response.json())

    # 部门添加
    def department_add(self, jsonData):
        app.HEADERS['Authorization'] = self.Authorization
        print(self.headers)
        return requests.post(url=self.url,
                             headers=app.HEADERS,
                             json=jsonData)
        # print(response.json())

    # 部门修改
    def department_modify(self, id, jsondata):
        self.url = self.url + "/" + id
        app.HEADERS['Authorization'] = self.Authorization
        return requests.put(url=self.url,
                            params=id,
                            headers=app.HEADERS,
                            json=jsondata)
        # print(response.json())

    # 部门删除
    def department_delete(self, id):
        self.url = self.url + '/' + id
        return requests.delete(url=self.url,
                               params=id,
                               headers=self.headers)
        # print(response.json())


if __name__ == '__main__':
    a = Department()
    body = {'name': '老蛋',
            'code': '333'}
    a.department_add(body)
