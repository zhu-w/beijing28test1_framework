# 导包
import requests
import app


# 创建类

class EmployeeApi:
    def __init__(self):
        self.url_employee = app.BASE_URL + "/api/sys/user"

    # 查询员工列表接口
    def query_employee_list(self, headers):
        return requests.get(url=self.url_employee, params={"page": "1", "size": "10"},
                            headers=headers)

    # 添加员工接口
    def add_employee(self, headers, jsonDate):
        return requests.post(url=self.url_employee,
                             headers=headers,
                             json=jsonDate)

    # 查询员工
    def query_emplyee(self, headers, target):
        query_url = self.url_employee + "/" + target
        print(f'查询员工url为:{query_url}')
        return requests.get(query_url,
                            headers=headers)

    def update_employee(self, target, headers, jsonDate):
        update_url = self.url_employee + "/" + target
        print(f'修改员工url为:{update_url}')
        print(update_url)
        return requests.put(url=update_url,
                            headers=headers,
                            json=jsonDate)

    def delete_employee(self, target, headers):
        delete_url = self.url_employee + "/" + target
        print("删除员工url为:", delete_url)
        requests.delete(url=delete_url,
                        headers=headers)
        '''json=jsonDate'''
# if __name__ == '__main__':
#     id = '1341646565709811712'
#     a = EmployeeApi()
#     a.query_employee_list(app.HEADERS,id)
