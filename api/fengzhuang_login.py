# 创建封装的类
class LoginApi():
    def __init__(self):
        pass

    # 封装的登录
    def login(self, session, jsonData):
        # 使用外部的传入的session对象发送请求,请求体从外部接收
        return session.post(url="http://ihrm-test.itheima.net/api/sys/login",
                            headers={"Content-Type": "application/json"},
                            json=jsonData)
    # 使用外部的传入的session对象发送请求,请求体从外部接收


if __name__ == '__main__':
    # 实例化对象
    a = LoginApi()
    # 实例化session
    import requests

    session = requests.Session()
    # 打印响应体
    print(a.login(session, {"jsonData数据"}))
