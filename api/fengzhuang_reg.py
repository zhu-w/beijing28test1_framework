import unittest

from data import data1


class RegTest(object):
    def __init__(self, session):
        self.session = session
        # print(self.session)

    def reg_method(self, data):
        self.session.get(url="http://tpshop-test.itheima.net/index.php?m=Home&c=User&a=verify&type=user_reg")
        response = self.session.post(url="http://tpshop-test.itheima.net/Home/User/reg.html",
                                     headers={"Content-Type": "application/x-www-form-urlencoded"},
                                     data=data)
        return response


if __name__ == '__main__':
    import requests

    session = requests.Session()
    a = RegTest(session)
    # response1 = a.reg_method({"scene": '1', "username": '13883388456', "verify_code": '8888',
    #                           "password": '123456', "password2": '123456'})
    response1 = a.reg_method(data1.data)
    print(response1.json())
    session.close()
