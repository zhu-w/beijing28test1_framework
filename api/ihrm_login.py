# 导包
import requests, app


# 创建封装的类
class LoginApi:
    def __init__(self):
        self.url_login = app.BASE_URL + "/api/sys/login"  # 定义登陆的URL
        self.headers = app.HEADERS  # 定义登陆的请求头

    # 编写封装的登陆接口
    def login(self, jsonData):
        return requests.post(url=self.url_login, headers=self.headers, json=jsonData)
