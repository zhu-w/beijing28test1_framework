b = {"case_name": "登陆成功",
     "file_request_data": {"mobile": "13800000002", "password": "123456"},
     "success": True,
     "code": 10000,
     "message": "操作成功！"}

# 转化所有的value为列表数据
print(b.values())

# 转化为元组数据
print(tuple(b.values()))

# 把转化后的元组数据，添加到空列表，形成列表元组数据
empty_list = []
for i in range(3):
    empty_list.append(tuple(b.values()))
print(empty_list)

