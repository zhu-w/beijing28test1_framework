# # 1 导包
# import unittest
# import HTMLTestRunner_PY3
# # 2 创建测试套件
# from script.ihrm_login_test import TestLogin
# from script.test_department import DepartmentTest
# from script.test_employee import LoginEmployee
# from script.test_login import TestLoginSuccess
#
# suite = unittest.TestSuite()
# # 3 将测试用例添加到测试套件
# suite.addTest(unittest.makeSuite(DepartmentTest))
# # 4 初始化runner
# with open('./report/report_department.html', 'wb') as f:
#     runner = HTMLTestRunner_PY3.HTMLTestRunner(f)
#     # 5 执行测试套件生成测试报告
#     runner.run(suite)

# 1 导包
import unittest,app,time
import HTMLTestRunner_PY3

# from script.test_emp_params import TestEmployeeParams
# 2 创建测试套件
from script.test_login_paramsized import TestLoginSuccess

suite = unittest.TestSuite()
# 3 将测试用例添加到测试套件
# suite.addTest(unittest.makeSuite(TestLoginSuccess)) # 要理解
suite.addTest(unittest.makeSuite(TestLoginSuccess)) # 要理解

# 4 初始化runner
with open(app.BASE_PATH + f'/report/report{time.strftime("%Y%m%d %H%M%S")}.html', 'wb') as f:
    runner = HTMLTestRunner_PY3.HTMLTestRunner(f)
    # 5 执行测试套件生成测试报告
    runner.run(suite)
