import json


# 编写函数转化json__参数化
def read_data(filename):
    # 打开对应文件
    with open(file=filename, mode="r", encoding='utf_8') as f:
        # 加载json文件
        jsondata = json.load(f)
        # 创建空列表盛放数据
        empty_list = []
        # 利用循环遍历列表中所有的元素
        for i in jsondata:
            print(i)
            # 将列表遍历出的字典  提取出其中的值  元组化后添加到准备的空列表中
            empty_list.append(tuple(i.values()))
        print(empty_list)
        return empty_list


# 调试代码
if __name__ == '__main__':
    # 查看读取出的数据是否为列表嵌套元组数据
    # 以便接下来用于参数化中
    # 参数化中的传参必须为元组
    read_data("./data/login_data.json")
