# 导包
import unittest

# 创建unittest框架
from api.ihrm_login import LoginApi


class TestLogin(unittest.TestCase):
    def setUp(self):
        self.pre_login = LoginApi()

    # 创建TestCase
    def test01_login_success(self):
        # jsonDate={"mobile":""}
        response = self.pre_login.login({"mobile": ""})
        print(response.json())

        self.assertEqual(200, response.status_code)
