# 导包
import unittest
import requests
from parameterized import parameterized
from api.fengzhuang_login import LoginApi


class TestLoginSuccess(unittest.TestCase):
    # 定义测试固件
    def setUp(self):
        # 实例化session
        self.session = requests.Session()
        self.login_api = LoginApi()

    def tearDown(self):
        # 关闭session
        self.session.close()

    # 定义测试函数
    @parameterized.expand("传递的数据")
    def test01_login_success(self):
        response = self.login_api.login(self.session, {"mobile": "13800000002",
                                                       "password": "123456"})
        print(f'登录结果为:{response.json()}')
        self.assertEqual(True, response.json().get("success"))  # 断言success
        self.assertEqual(10000, response.json().get("code"))  # 断言code
        self.assertIn("操作成功", response.json().get("message"))  # 断言message
        self.assertEqual(200, response.status_code)  # 断言响应状态码
