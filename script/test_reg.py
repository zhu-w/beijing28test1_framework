# 导包
import unittest

from api.fengzhuang_reg import RegTest
from data import data1


class TestReg(unittest.TestCase):

    def setUp(self):
        import requests
        self.session = requests.Session()

    def tearDown(self):
        self.session.close()

    def test_method(self):
        reg1 = RegTest(self.session)
        resopnse = reg1.reg_method(data1.data)
        print(resopnse.json())
        self.assertEqual("账号已存在", resopnse.json().get('msg'))
