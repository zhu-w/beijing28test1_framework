import unittest

import requests
from parameterized import parameterized

import app
from api.fengzhuang_login import LoginApi
from utils import read_data


class TestLoginSuccess(unittest.TestCase):
    # 定义测试固件
    def setUp(self):
        # 实例化session
        self.session = requests.Session()
        self.login_api = LoginApi()

    def tearDown(self):
        # 关闭session
        self.session.close()

    # 定义测试函数
    @parameterized.expand(read_data(app.BASE_PATH + "/data/login_data.json"))
    def test01_login_success(self,case_name,file_request_data,success,code,message):
        response = self.login_api.login(self.session,file_request_data )
        print(f'{case_name}登录结果为:{response.json()}')
        self.assertEqual(success, response.json().get("success"))  # 断言success
        self.assertEqual(code, response.json().get("code"))  # 断言code
        self.assertIn(message, response.json().get("message"))  # 断言message
        self.assertEqual(200, response.status_code)  # 断言响应状态码