import unittest
import app
from parameterized import parameterized
from api.ihrm_employee import EmployeeApi
from api.ihrm_login import LoginApi
from utils import read_data


class LoginEmployee(unittest.TestCase):

    def setUp(self):
        # 实例化封装登录类
        self.login = LoginApi()
        # 实例化员工类
        self.emp = EmployeeApi()

    def tearDown(self):
        pass

    @parameterized.expand(read_data(app.BASE_PATH + "/data/login.json"))
    def test01_login(self, jsondate):
        response = self.login.login(jsondate)
        app.HEADERS['Authorization'] = "Bearer " + response.json().get('data')
        # print(app.HEADERS)

    def test02_query_emp_list(self):
        # 调用查询员工列表
        response = self.emp.query_employee_list(app.HEADERS)
        # 查看结果
        print(response.json())

    @parameterized.expand(read_data(app.BASE_PATH + "/data/add_emp.json"))
    def test03_add_employee(self,request_body,success,code,message,resposne_code):
        print(app.HEADERS)
        response = self.emp.add_employee(app.HEADERS, request_body)
        # print(response.json())
        app.EMP_ID = response.json().get('data').get('id')
        print(app.EMP_ID)

    @parameterized.expand(read_data(app.BASE_PATH + "/data/query_one_emp.json"))
    def test04_query_employee(self,success,code,message,resposne_code):
        response = self.emp.query_emplyee(app.HEADERS, app.EMP_ID)
        print(response.json())

    @parameterized.expand(read_data(app.BASE_PATH + "/data/update_emp.json"))
    def test05_modify_employee(self,request_body,success,code,message,resposne_code):
        response = self.emp.update_employee(app.EMP_ID, app.HEADERS, request_body)
        print(response.json())

    @parameterized.expand(read_data(app.BASE_PATH + "/data/delete_emp.json"))
    def test06_delete_employee(self,success,code,message,resposne_code):
        response = self.emp.delete_employee(app.EMP_ID, app.HEADERS)
        print(response)
