import unittest
from parameterized import parameterized

import app
import data
from api.fengzhuang_department import Department
from utils import read_data


class DepartmentTest(unittest.TestCase):
    def setUp(self):
        self.a = Department()

    def tearDown(self):
        pass

    # @parameterized.expand()
    def test01_method(self):
        response = self.a.department_list_query()
        print(response.json())

    # @parameterized.expand()
    def test02_method(self):
        id = app.DEPARTMENT_ID
        response = self.a.department_query(id)
        print(response.json())

    @parameterized.expand(read_data(app.BASE_PATH + "/data/add_department.json"))
    def test03_method(self, jsonDate):
        response = self.a.department_add(jsonDate)
        app.DEPARTMENT_ID = response.json().get('data').get('id')
        print(app.DEPARTMENT_ID)

    @parameterized.expand(read_data(app.BASE_PATH + "/data/department_modify.json"))
    def test04_method(self, jsondata):
        response = self.a.department_modify(app.DEPARTMENT_ID, jsondata)
        print(response.json())

    #
    # #
    # @parameterized.expand()
    def test05_method(self):
        response = self.a.department_delete(app.DEPARTMENT_ID)
        print(response.json())
